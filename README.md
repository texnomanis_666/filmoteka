This application is designed to make it easy to sort video files on your local disk. It contains the ability to play the desired file. The video player and folder with video files are specified in the settings.

The application is developed using the cross-platform fyne framework, which allows it to be used on different operating systems (but it must be compiled first).

You will need gcc or its windows version MinGW. I used version 13.1.0.

The file and dir selection dialogs are taken from this library.
github.com/sqweek/dialog

