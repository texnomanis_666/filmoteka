package main

import (
	"encoding/json"
	"io/ioutil"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"

	"fmt"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	dia "github.com/sqweek/dialog"
)

type Config struct {
	VideoPlayer string
	LightTheme  bool
	SearchType  string
	FilmsDir    string
}

// сохранить конфиг
func (c *Config) save() {

	file, err := json.Marshal(c)

	if err != nil {
		panic(err)
	} else {

		// save
		err = ioutil.WriteFile("./config.json", file, 0644)
		if err != nil {
			fmt.Println(err)
		}
	}
}

// загрузить конфиг
func (c *Config) load() bool {

	rez := true
	file, err := ioutil.ReadFile("./config.json")
	if err != nil {
		fmt.Println(err)
		rez = false
	}

	err = json.Unmarshal([]byte(file), c)

	if err != nil {
		fmt.Println(err)
		rez = false
	}
	return rez
}

type Tfilm struct {
	FullPath                  string
	Dir                       string
	Name                      string
	Ext                       string
	Year                      string
	NameRUS                   string
	NameOriginal              string
	Comment1                  string
	Comment2                  string
	Index                     int
	FilterFirstSimbolRUS      string
	FilterFirstSimbolOriginal string
}

// добавить и распарсить имя
func (f *Tfilm) ParseName(fullpath string) {

	if fullpath != "" {
		f.FullPath = fullpath
	} else {
		fullpath = f.FullPath
	}

	//fullpath = string(fullpath)

	f.Ext = filepath.Ext(fullpath)
	fullpath = strings.ReplaceAll(fullpath, f.Ext, "")

	f.Dir = filepath.Dir(fullpath)
	f.Name = filepath.Base(fullpath)

	// год
	f.Year = "Год не указан"
	if len(f.Name) > 4 {
		f.Year = f.Name[len(f.Name)-4:]
	}

	i1 := strings.Index(f.Name, "(")
	i2 := strings.Index(f.Name, ")")
	if i1 != -1 && i2 > i1 {
		f.NameOriginal = f.Name[i1+1 : i2]
		// название на русском
		f.NameRUS = f.Name[:i1-1]
	}

	// второй коментарий

	f.Comment2 = strings.Replace(f.Name, f.NameRUS, "", 1)
	f.Comment2 = strings.Replace(f.Comment2, f.NameOriginal, "", 1)
	f.Comment2 = strings.Replace(f.Comment2, "_"+f.Year, "", 1)
	f.Comment2 = strings.Replace(f.Comment2, "_()_", "", 1)
	f.Comment2 = strings.Replace(f.Comment2, "()", "", 1)
	f.Comment2 = strings.Replace(f.Comment2, "[", "", 1)
	f.Comment2 = strings.Replace(f.Comment2, "]", "", 1)

	// 1-ый коментарий
	i1 = strings.Index(f.NameRUS, "[")
	i2 = strings.Index(f.NameRUS, "]")
	if i1 != -1 && i2 != -1 {
		f.Comment1 = f.NameRUS[i1+1 : i2]
	}
	f.NameRUS = strings.Replace(f.NameRUS, f.Comment1, "", 1)
	f.NameRUS = strings.Replace(f.NameRUS, "_[]", "", 1)

	// замена нижнего подчёркивания на пробелы
	f.NameRUS = strings.ReplaceAll(f.NameRUS, "_", " ")
	f.NameOriginal = strings.ReplaceAll(f.NameOriginal, "_", " ")
	f.Comment1 = strings.ReplaceAll(f.Comment1, "_", " ")
	f.Comment2 = strings.ReplaceAll(f.Comment2, "_", " ")

	if f.NameRUS == "" {
		f.NameRUS = f.Name
	}

	isYear, _ := regexp.MatchString("[0-9]{4}", f.Year)

	if isYear == false {
		f.Year = "Год не указан"
	}

	if f.NameRUS != "" {
		f.FilterFirstSimbolRUS = f.NameRUS[:1]
	}

	if f.NameOriginal != "" {
		f.FilterFirstSimbolOriginal = f.NameOriginal[:1]
	}

}

func GetFilms(dir string) []string {

	var rez []string

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		fmt.Println(err)
	}

	for _, f := range files {

		name := strings.ToUpper(filepath.Ext(f.Name()))

		if (name == ".MP4") ||
			(name == ".AVI") ||
			(name == ".MKV") ||
			(name == ".FLV") ||
			(name == ".WMV") ||
			(name == ".MOV") ||
			(name == ".M4V") {

			rez = append(rez, dir+"//"+f.Name())

		}
	}
	return rez
}

func MakeFilmsList(list []string) []Tfilm {
	var ff Tfilm
	var filmlist []Tfilm
	for i := 0; i < len(list); i++ {
		ff.Dir = ""
		ff.Name = ""
		ff.Ext = ""
		ff.Year = ""
		ff.NameRUS = ""
		ff.NameOriginal = ""
		ff.Comment1 = ""
		ff.Comment2 = ""
		ff.Index = i
		ff.ParseName(list[i])
		filmlist = append(filmlist, ff)
	}

	return filmlist
}

func CreateFilmBlock(f Tfilm, c Config) *fyne.Container {

	texts := container.NewVBox()

	if f.NameRUS != "" {
		nameruslabel := widget.NewLabel(f.NameRUS)
		texts.Add(nameruslabel)
	}

	if f.Comment1 != "" {
		coment1label := widget.NewLabel(f.Comment1)
		texts.Add(coment1label)
	}

	if f.Year != "" {
		yarlabel := widget.NewLabel(f.Year)
		texts.Add(yarlabel)
	}

	if f.Comment2 != "" && len(f.Comment2) > 2 {
		coment2label := widget.NewLabel(f.Comment2)
		texts.Add(coment2label)
	}

	if f.NameOriginal != "" {
		nameoriginallabel := widget.NewLabel(f.NameOriginal)
		texts.Add(nameoriginallabel)
	}

	if f.Ext != "" {
		extlabel := widget.NewLabel(f.Ext)
		texts.Add(extlabel)
	}

	seebtn := widget.NewButton("Смотреть фильм", func() {

		cmd := exec.Command(c.VideoPlayer, f.FullPath)

		err := cmd.Start()
		if err != nil {
			fmt.Println(err)
		}

	})
	texts.Add(seebtn)

	return texts

}

func ApplyFilter(year, text string, films []Tfilm, c Config) []Tfilm {
	var name string
	var rez []Tfilm
	var i int

	text = strings.ToUpper(text)
	text = strings.ReplaceAll(text, "_", " ")
	text = strings.ReplaceAll(text, "  ", " ")

	if text == "" {

		for i = 0; i < len(films); i++ {
			name = strings.ToUpper(films[i].Name)
			name = strings.ReplaceAll(name, "_", " ")
			name = strings.ReplaceAll(name, "  ", " ")

			if films[i].Year == year {
				rez = append(rez, films[i])
			}

		}
	}

	if year == "Год не указан" || year == "" {

		for i = 0; i < len(films); i++ {
			name = strings.ToUpper(films[i].Name)
			name = strings.ReplaceAll(name, "_", " ")
			name = strings.ReplaceAll(name, "  ", " ")

			if strings.Contains(name, text) {
				rez = append(rez, films[i])
			}

		}
	}

	if c.SearchType == "И" {

		if year != "Год не указан" && year != "" && text != "" {

			for i = 0; i < len(films); i++ {
				name = strings.ToUpper(films[i].Name)
				name = strings.ReplaceAll(name, "_", " ")
				name = strings.ReplaceAll(name, "  ", " ")

				if strings.Contains(name, text) && films[i].Year == year {
					rez = append(rez, films[i])
				}

			}
		}
	}

	if c.SearchType == "ИЛИ" {

		if year != "Год не указан" && year != "" && text != "" {

			for i = 0; i < len(films); i++ {
				name = strings.ToUpper(films[i].Name)
				name = strings.ReplaceAll(name, "_", " ")
				name = strings.ReplaceAll(name, "  ", " ")

				if strings.Contains(name, text) || films[i].Year == year {
					rez = append(rez, films[i])
				}

			}
		}
	}

	return rez
}

func UpdateLenta(container *fyne.Container, FilmsList []Tfilm, c Config) {
	container.Objects = []fyne.CanvasObject{}
	for i := 0; i < len(FilmsList); i++ {
		container.Add(CreateFilmBlock(FilmsList[i], c))
	}
	container.Refresh()
}

func main() {

	var c Config
	var AllFilmsList, FilterFilmList []Tfilm

	a := app.New()
	a.Settings().SetTheme(theme.DarkTheme())
	w := a.NewWindow("Фильмотека")
	w.Resize(fyne.NewSize(800, 600))
	w_config := a.NewWindow("Настройки фильмотеки")
	w_config.Resize(fyne.NewSize(800, 600))

	FilmBoxes := container.NewVBox(widget.NewLabel("Тут должен быть список фильмов"),
		widget.NewLabel("открой настройки и укажи папку с фильмами"))
	Lenta := container.NewVScroll(FilmBoxes)

	// меню настройки
	cm_label1 := widget.NewLabel("Видеоплеер")
	сm_entry1 := widget.NewEntry()
	сm_btn1 := widget.NewButton("Выбрать видеоплеер", func() {

		filename, err := dia.File().Filter("*", "*").Load()

		if err == nil {
			сm_entry1.SetText(filename)
		}

	})

	cm_label2 := widget.NewLabel("Папка с фильмами")
	сm_entry2 := widget.NewEntry()
	сm_btn2 := widget.NewButton("Выбрать папку с фильмами", func() {

		directory, err := dia.Directory().Title("Папка с фильмами").Browse()

		if err == nil {
			сm_entry2.SetText(directory)
		}
	})

	cm_darktheme_check := widget.NewCheck("Светлая тема", func(b bool) {
		c.LightTheme = b

		if c.LightTheme == false {
			a.Settings().SetTheme(theme.DarkTheme())
		} else {
			a.Settings().SetTheme(theme.LightTheme())
		}
	})

	сm_btn3 := widget.NewButton("Сохранить настройки", func() {

		c.FilmsDir = сm_entry2.Text
		c.VideoPlayer = сm_entry1.Text

		c.save()

		w_config.Hide()
	})

	ConfigMenu := container.NewVBox(
		cm_label1,
		сm_entry1,
		сm_btn1,
		cm_label2,
		сm_entry2,
		сm_btn2,

		cm_darktheme_check,

		сm_btn3,
	)

	// боковое меню
	lm_label1 := widget.NewLabel("Фильтры")
	lm_label2 := widget.NewLabel("Год:")
	lm_label4 := widget.NewLabel("Тип поиска:")

	// строка поиска
	lm_entry1 := widget.NewEntry()
	lm_entry1.SetPlaceHolder("Что искать ?")

	// выбор года
	lm_rg1 := widget.NewRadioGroup([]string{}, func(s string) {

		FilterFilmList = ApplyFilter(s, lm_entry1.Text, AllFilmsList, c)

		UpdateLenta(FilmBoxes, FilterFilmList, c)
	})

	rg1_lenta := container.NewVScroll(container.NewVBox(lm_rg1))

	lm_entry1.OnChanged = func(s string) {

		FilterFilmList = ApplyFilter(lm_rg1.Selected, s, AllFilmsList, c)

		UpdateLenta(FilmBoxes, FilterFilmList, c)

	}

	rg1_lenta.SetMinSize(fyne.NewSize(0, 7*38))

	// тип поиска
	lm_rg3 := widget.NewRadioGroup([]string{"ИЛИ", "И"}, func(s string) {

		c.SearchType = s

		FilterFilmList = ApplyFilter(lm_rg1.Selected, s, AllFilmsList, c)

		UpdateLenta(FilmBoxes, FilterFilmList, c)

	})
	lm_rg3.SetSelected("ИЛИ")

	lm_btn1 := widget.NewButton("Сброс фильтров", func() {
		lm_entry1.SetText("")
		lm_rg1.SetSelected("")
		lm_rg3.SetSelected("ИЛИ")
		UpdateLenta(FilmBoxes, AllFilmsList, c)
	})

	lm_btn2 := widget.NewButton("Настройки", func() {

		w_config.Show()

	})

	LeftMenu := container.NewVBox(
		lm_btn2,
		lm_btn1,
		lm_entry1,
		lm_label1,
		lm_label2,
		rg1_lenta,
		lm_label4,
		lm_rg3,
	)

	w_config.SetContent(ConfigMenu)

	c.load()

	if c.LightTheme == false {
		a.Settings().SetTheme(theme.DarkTheme())
	} else {
		a.Settings().SetTheme(theme.LightTheme())
	}

	сm_entry1.SetText(c.VideoPlayer)
	сm_entry2.SetText(c.FilmsDir)

	cm_darktheme_check.SetChecked(c.LightTheme)

	AllFilesList := GetFilms(c.FilmsDir)
	AllFilmsList = MakeFilmsList(AllFilesList)

	FilmBoxes = container.NewVBox(widget.NewLabel("Тут должен быть список фильмов"),
		widget.NewLabel("открой настройки и укажи папку с фильмами"))
	Lenta = container.NewVScroll(FilmBoxes)

	if len(AllFilmsList) > 0 {
		UpdateLenta(FilmBoxes, AllFilmsList, c)
	}

	for i := 0; i < len(AllFilmsList); i++ {
		lm_rg1.Append(AllFilmsList[i].Year)
	}

	Lenta = container.NewVScroll(FilmBoxes)

	w.SetContent(container.NewHSplit(
		LeftMenu,
		Lenta,
	))

	w.ShowAndRun()

}
